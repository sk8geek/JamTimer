package uk.co.channele.jamtimer;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.NumberPicker;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;


public class PeriodClock extends DialogFragment {

    private SettingsListener settingsListener;
    private int periodMinutes = 0;
    private int periodSeconds = 0;
    private boolean resetIntervalCounter = false;
    private NumberPicker minutePicker;
    private NumberPicker secondPicker;
    private CheckBox resetIntervalCounterCheckBox;


    public interface SettingsListener {
        void onDialogPositiveClick(DialogFragment dialog);
        void onDialogNegativeClick(DialogFragment dialog);
    }

    public static PeriodClock newInstance(int id) {
        return new PeriodClock();
    }


    /**
     * Returns the values for minutes and seconds in a bundle.
     * @return a bundle containing the minute/second values
     */
    public Bundle getState() {
        Bundle state = new Bundle();
        state.putInt("periodMinutes", periodMinutes);
        state.putInt("periodSeconds", periodSeconds);
        state.putBoolean("resetIntervalCounter", resetIntervalCounter);
        return state;
    }


    @Override
    public void onAttach(@NonNull Activity activity) {
        super.onAttach(activity);
        try {
            settingsListener = (SettingsListener)activity;
        } catch (ClassCastException ccex) {
            throw new ClassCastException(activity + " doesn't implement SettingsListener");
        }
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle state) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        Dialog tipex;
        View view = inflater.inflate(R.layout.adjust_period_clock, null);
        builder.setView(view);
        minutePicker = view.findViewById(R.id.periodMinutes);
        minutePicker.setMinValue(0);
        minutePicker.setMaxValue(30);
        minutePicker.setValue(periodMinutes);
        secondPicker = view.findViewById(R.id.periodSeconds);
        secondPicker.setMinValue(0);
        secondPicker.setMaxValue(59);
        secondPicker.setValue(periodSeconds);
        resetIntervalCounterCheckBox = view.findViewById(R.id.resetIntervalCounter);
        builder.setPositiveButton(R.string.select, (dialog, id) -> {
            periodMinutes = minutePicker.getValue();
            periodSeconds = secondPicker.getValue();
            resetIntervalCounter = resetIntervalCounterCheckBox.isChecked();
            settingsListener.onDialogPositiveClick(PeriodClock.this);
        });
        builder.setNegativeButton(R.string.cancel, (dialog, id) -> {
            if (PeriodClock.this.getDialog() != null) {
                PeriodClock.this.getDialog().cancel();
            }
        });
        tipex = builder.create();
        return tipex;
    }


    @Override
    public void onSaveInstanceState(Bundle state) {
        state.putInt("periodMinutes", periodMinutes);
        state.putInt("periodSeconds", periodSeconds);
        state.putBoolean("resetIntervalCounter", resetIntervalCounter);
        super.onSaveInstanceState(state);
    }


    /**
     * Sets the minute/second values from those in the supplied bundle.
     * Note that this method does NOT set the NumberPicker values. This is done
     * when the dialog is created.
     * @param args the bundle containing the values
     */
    @Override
    public void setArguments(Bundle args) {
        if (args != null) {
            periodMinutes = args.getInt("periodMinutes");
            periodSeconds = args.getInt("periodSeconds");
        }
    }

}
