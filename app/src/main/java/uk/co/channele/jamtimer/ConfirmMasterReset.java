package uk.co.channele.jamtimer;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

public class ConfirmMasterReset extends DialogFragment {

    private ConfirmMasterReset.SettingsListener settingsListener;

    public interface SettingsListener {
        void onDialogPositiveClick(DialogFragment dialog);
        void onDialogNegativeClick(DialogFragment dialog);
    }

    public static ConfirmMasterReset newInstance(int id) {
        return new ConfirmMasterReset();
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle state) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.confirmMasterReset).setPositiveButton(R.string.ok, (dialog, id) -> {
            settingsListener.onDialogPositiveClick(ConfirmMasterReset.this);
        }).setNegativeButton(R.string.cancel, (dialog, id) -> {
            if (ConfirmMasterReset.this.getDialog() != null) {
                ConfirmMasterReset.this.getDialog().cancel();
            }
        });
        return builder.create();
    }

    @Override
    public void onAttach(@NonNull Activity activity) {
        super.onAttach(activity);
        try {
            settingsListener = (ConfirmMasterReset.SettingsListener)activity;
        } catch (ClassCastException ccex) {
            throw new ClassCastException(activity + " doesn't implement SettingsListener");
        }
    }

}
