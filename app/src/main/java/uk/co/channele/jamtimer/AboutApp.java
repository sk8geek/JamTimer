package uk.co.channele.jamtimer;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;


public class AboutApp extends DialogFragment {

    public static AboutApp newInstance(int id) {
        AboutApp picker = new AboutApp();
        Bundle bundle = new Bundle();
        bundle.putInt("id", id);
        picker.setArguments(bundle);
        return picker;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        Dialog aboutApp;
        @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.about_app, null);
        builder.setView(view);
        builder.setNeutralButton(R.string.ok, (dialog, id) -> {
            if (AboutApp.this.getDialog() != null) {
                AboutApp.this.getDialog().cancel();
            }
        });
        aboutApp = builder.create();
        return aboutApp;
    }
}
