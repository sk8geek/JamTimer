package uk.co.channele.jamtimer;

import static uk.co.channele.jamtimer.Activity.*;
import static uk.co.channele.jamtimer.Counter.FIFTEEN_MINUTES;
import static uk.co.channele.jamtimer.Counter.FIVE_SECONDS;
import static uk.co.channele.jamtimer.Counter.ONE_MINUTE;
import static uk.co.channele.jamtimer.Counter.ONE_SECOND;
import static uk.co.channele.jamtimer.Counter.TWO_MINUTES;

import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.lifecycle.ViewModel;

public class TimerViewModel extends ViewModel {

    private int periodDurationMinutes = 30;
    private int lineUpDurationSeconds = 30;
    private boolean allowImmediateStart = false;
    // state variables
    private int period = 0;
    private int jam = 0;
    private boolean paused = true;
    private boolean timeoutFlag = false;
    private boolean officialTimeout = false;
    private Activity currentActivity = NONE;
    private Counter periodCounter = new Counter(periodDurationMinutes * ONE_MINUTE);
    private Counter activityCounter = new Counter(lineUpDurationSeconds * ONE_SECOND);


    public TimerViewModel() {}


    int getPeriod() {
        return period;
    }

    void setPeriod(int period) {
        this.period = period;
    }


    int getJam() {
        return jam;
    }

    void clearJamCount() {
        this.jam = 0;
    }

    void incrementJam() {
        jam++;
    }

    void resetJam() {
        currentActivity = INTERVAL;
        jam--;
        paused = true;
        periodCounter.refund(TWO_MINUTES - activityCounter.remaining());
        periodCounter.setRunning(false);
        activityCounter.reset(FIVE_SECONDS, 0);
    }


    boolean isPaused() {
        return paused;
    }

    void setPaused(boolean paused) {
        this.paused = paused;
    }

    boolean isTimeoutFlag() {
        return timeoutFlag;
    }

    void clearTimeoutFlag() {
        this.timeoutFlag = false;
    }

    Activity getCurrentActivity() {
        return currentActivity;
    }

    void setCurrentActivity(Activity currentActivity) {
        this.currentActivity = currentActivity;
        switch (this.currentActivity) {
            case INTERVAL:
                if (timeoutFlag) {
                    if (officialTimeout) {
                        if (activityCounter.remaining() < FIVE_SECONDS) {
                            activityCounter.reset(FIVE_SECONDS, 0);
                        } else {
                            timeoutFlag = false;
                            officialTimeout = false;
                            activityCounter.start();
                        }
                    } else {
                        activityCounter.resetAndStart(lineUpDurationSeconds * ONE_SECOND, FIVE_SECONDS);
                        paused = false;
                    }
                } else {
                    activityCounter.resetAndStart(lineUpDurationSeconds * ONE_SECOND, FIVE_SECONDS);
                }
                break;
            case JAM:
                activityCounter.resetAndStart(TWO_MINUTES, 0);
                break;
            case TEAM_TIMEOUT:
                activityCounter.resetAndStart(ONE_MINUTE, 0);
                periodCounter.setRunning(false);
                timeoutFlag = true;
                officialTimeout = false;
                break;
            case OFFICIAL_TIMEOUT:
                timeoutFlag = true;
                officialTimeout = true;
                periodCounter.setRunning(false);
                paused = true;
                break;
            case HALFTIME:
                activityCounter.resetAndStart(FIFTEEN_MINUTES, 0);
                break;
            case NONE:
            default:
                activityCounter.reset(lineUpDurationSeconds * ONE_SECOND, 0);
                break;
        }
    }


    Counter getActivityCounter() {
        return activityCounter;
    }

    String getActivityDisplayTime() {
        return activityCounter.getDisplayTime();
    }

    void resetActivityCounter(int initialValue, int warningValue, boolean start) {
        if (start) {
            activityCounter.resetAndStart(initialValue, warningValue);
        } else {
            activityCounter.reset(initialValue, warningValue);
        }
    }

    boolean isActivityCounterRunning() {
        return activityCounter.isRunning();
    }

    boolean isActivityCounterDisplayTimeChanged() {
        return activityCounter.isDisplayTimeChanged();
    }

    boolean isActivityCounterWarningLevel() {
        return activityCounter.isWarningLevel();
    }

    boolean isActivityCounterExpired() {
        return activityCounter.isExpired();
    }

    void decrementActivityCounter() {
        activityCounter.decrement();
    }

    void resetPeriodCounter() {
        periodCounter.reset(periodDurationMinutes * ONE_MINUTE);
    }

    void resetPeriodCounterMinutesSeconds(int minutes, int seconds) {
        periodCounter.resetMinutesSeconds(minutes, seconds);
    }

    void startPeriodCounter() {
        periodCounter.setRunning(true);
    }

    int getPeriodRemaining() {
        return periodCounter.remaining();
    }

    int getPeriodDurationMinutes() {
        return periodDurationMinutes;
    }

    int getLineUpDurationSeconds() {
        return lineUpDurationSeconds;
    }

    void setPeriodDurationMinutes(int minutes) {
        periodDurationMinutes = minutes;
        periodCounter.reset(periodDurationMinutes * ONE_MINUTE, 0);
    }

    void setLineUpDurationSeconds(int seconds) {
        lineUpDurationSeconds = seconds;
    }

    boolean getAllowImmediateStart() {
        return allowImmediateStart;
    }

    void setAllowImmediateStart(boolean autoStart) {
        allowImmediateStart = autoStart;
    }

    String getPeriodDisplayTime() {
        return periodCounter.getDisplayTime();
    }

    boolean isPeriodCounterRunning() {
        return periodCounter.isRunning();
    }

    boolean isPeriodCounterExpired() {
        return periodCounter.isExpired();
    }

    boolean isPeriodCounterDisplayTimeChanged() {
        return periodCounter.isDisplayTimeChanged();
    }

    void decrementPeriodCounter() {
        periodCounter.decrement();
    }

    void restoreState(Bundle state) {
        if (state != null) {
            currentActivity = Activity.valueOf(state.getString("activity"));
            paused = state.getBoolean("paused");
            timeoutFlag = state.getBoolean("timeout");
            periodCounter = (Counter)state.getSerializable("periodCounter");
            activityCounter = (Counter)state.getSerializable("activityCounter");
            period = state.getInt("period");
            jam = state.getInt("jam");
            periodDurationMinutes = state.getInt("periodDuration");
            lineUpDurationSeconds = state.getInt("lineUpDuration");
            allowImmediateStart = state.getBoolean("allowImmediateStart");
        }
    }


    void saveInstanceState(Bundle state) {
        state.putBoolean("paused", paused);
        state.putBoolean("timeout", timeoutFlag);
        state.putString("activity", currentActivity.name());
        state.putSerializable("periodCounter", periodCounter);
        state.putSerializable("activityCounter", activityCounter);
        state.putInt("period", period);
        state.putInt("jam", jam);
        state.putInt("periodDuration", periodDurationMinutes);
        state.putInt("lineUpDuration", lineUpDurationSeconds);
        state.putBoolean("allowImmediateStart", allowImmediateStart);
    }


    /**
     * Resets all counters and jammer statuses.
     */
    void masterReset() {
        paused = true;
        period = 0;
        jam = 0;
        timeoutFlag = false;
        currentActivity = NONE;
        // counters
        activityCounter.reset(lineUpDurationSeconds * ONE_SECOND, FIVE_SECONDS);
        periodCounter.reset(periodDurationMinutes * ONE_MINUTE, 0);
    }

    void retrievePreferences(SharedPreferences prefs) {
        setPeriodDurationMinutes(prefs.getInt("periodDuration", 30));
        setLineUpDurationSeconds(prefs.getInt("lineUpDuration", 30));
        setAllowImmediateStart(prefs.getBoolean("allowImmediateStart", false));
    }

    /**
     * Save application preferences
     */
    void savePreferences(SharedPreferences prefs) {
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putInt("periodDuration", periodDurationMinutes);
        prefsEditor.putInt("lineUpDuration", lineUpDurationSeconds);
        prefsEditor.putBoolean("allowImmediateStart", allowImmediateStart);
        prefsEditor.apply();
    }
}
