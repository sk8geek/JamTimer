/* 
 * @(#) Counter.java	1.0	2014/03/05
 * 
 * Copyright 2013,2014 Steven J Lilley
 *
 * A counter, used to count down. This is part of the JamTimer application.
 *
 * JamTimer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * steven@channel-e.co.uk
 */
package uk.co.channele.jamtimer;

import java.io.Serializable;

/**
 * A countdown class to be used by timers. It is anticipated that the counters
 * will work in tenths of a second, hence a default resetValue of 600, equivalent
 * to one minute.
 */
class Counter implements Serializable {

	// Timings are in tenths of a second
	public static final int ONE_SECOND = 10;
	public static final int FIVE_SECONDS = 5 * ONE_SECOND;
	public static final int THIRTY_SECONDS = 30 * ONE_SECOND;
	public static final int ONE_MINUTE = 2 * THIRTY_SECONDS;
	public static final int TWO_MINUTES = 2 * ONE_MINUTE;
	public static final int FIFTEEN_MINUTES = 15 * ONE_MINUTE;
	public static final int THIRTY_MINUTES = 30 * ONE_MINUTE;
	private int warningValue = 0;
	private int resetValue;
	private int remaining = 600;
	private boolean expired = false;
	private boolean running = false;
	private boolean displayTimeChanged = false;
	private final boolean displayTenths = false;

	Counter(int initValue) {
		resetValue = initValue;
	}

	/**
	 * Decrement the counter, or reset it if it times out.
	 * NB If the counter is not set to run (ie paused) then it will not count down.
	 */
	void decrement() {
		if (remaining > 0) {
			if (running) {
				remaining--;
			}
		} else {
			expired = true;
		}
		float accurateRemaining = (float)remaining / 10;
		if (!displayTenths) {
            displayTimeChanged = accurateRemaining == ((float) remaining / 10);
		}
	}

	/**
	 * Get the time, in seconds, that the timer has to run.
	 * The value is rounded down.
	 * @return the number of seconds remaining as a String
	 */
	String getDisplayTime() {
		StringBuilder h = new StringBuilder();
		int minutes = (int)(remaining / Counter.ONE_MINUTE);
		h.append(minutes).append(":");
		int tenths = remaining - (minutes * Counter.ONE_MINUTE);
		int seconds = (int)(tenths / 10);
		if (seconds < 10) {
			h.append("0");
		}
		h.append(seconds);
		if (displayTenths) {
			h.append(".").append(tenths);
		}
		return h.toString();
	}

	/**
	 * Did the last decrement change the display time?
	 * The counter decrements every tenth of a second, but the time is 
	 * displayed in seconds. In most cases the display time remains the same
	 * as the previous value. When the last decrement caused a change to 
	 * the next second this method returns true.
	 * @return true if the last decrement was into a new second
	 */
	boolean isDisplayTimeChanged() {
		return displayTimeChanged;
	}

	/**
	 * Has the counter expired?
	 * @return true if the counter expired
	 */
	boolean isExpired() {
		return expired;
	}

	/**
	 * Ask if the counter is at/below the warning level
	 * @return true if warning level reached/passed
	 */
	boolean isLowLevel() {
        return remaining > 0 && remaining <= warningValue;
	}

	/**
	 * Test if the counter is running.
	 * @return true if running
	 */
	boolean isRunning() {
		return running;
	}

	/**
	 * Test to see if the warning level has been reached
	 * @return true if current value is the warning level
	 */
	public boolean isWarningLevel() {
        return remaining == warningValue;
    }

	/**
	 * Add the given time back to the counter.
	 * If the refund would cause the counter to exceed the resetValue the count
	 * is rounded down to the resetValue.
	 * @param refund the amount to add back to the counter
	 */
	void refund(int refund) {
		remaining += refund;
		if (remaining > resetValue) {
			remaining = resetValue;
		}
	}

	/**
	 * Get the time remaining, in tenths of a second.
	 * @return time remaining in tenths of a second
	 */
	int remaining() {
		return remaining;
	}

	/**
	 * Reset the counter to the given value.
	 */
	void reset(int duration) {
		resetValue = duration;
		remaining = resetValue;
		running = false;
		expired = false;
	}

	/**
	 * Reset the counter to the given value and set it running
	 * NB this method may be renamed/replace/combine reset and start methods
	 *    if I find that some methods aren't being used.
	 * @param reset the reset value
	 * @param warning the warning value
	 */
	void reset(int reset, int warning) {
		warningValue = warning;
		resetValue = reset;
		remaining = resetValue;
		running= false;
		expired = false;
	}

	/**
	 * Reset the counter to the given value and set it running
	 * NB this method may be renamed/replace/combine reset and start methods
	 *    if I find that some methods aren't being used.
	 * @param reset the reset value
	 * @param warning the warning value
	 */
	void resetAndStart(int reset, int warning) {
		warningValue = warning;
		resetValue = reset;
		remaining = resetValue;
		running = true;
		expired = false;
		displayTimeChanged = true;
	}

	void resetMinutesSeconds(int minutes, int seconds) {
		remaining = minutes * ONE_MINUTE + seconds * ONE_SECOND;
		if (remaining > resetValue) {
			remaining = resetValue;
		}
		if (remaining < 0) {
			remaining = 0;
		}
	}

	/**
	 * Mark the timer as running.
	 */
	void start() {
		running = true;
		expired = false;
		displayTimeChanged = true;
	}

	/**
	 * Set the run state of the counter
	 * @param run true is the counter should run, false to pause it
	 */
	void setRunning(boolean run) {
		running = run;
	}

}
