package uk.co.channele.jamtimer;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.NumberPicker;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;


public class TimerOptions extends DialogFragment {

    private SettingsListener settingsListener;
    private Integer periodDurationMinutes = 30;
    private Integer lineUpDurationSeconds = 30;
    private Boolean allowImmediateJamStart = false;
    private NumberPicker periodDurationPicker;
    private NumberPicker lineUpDurationPicker;
    private CheckBox allowImmediateStartCheckBox;

    public interface SettingsListener {
        void onDialogPositiveClick(DialogFragment dialog);
        void onDialogNegativeClick(DialogFragment dialog);
    }

    public static TimerOptions newInstance(int id) {
        return new TimerOptions();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle state) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        Dialog settings;
        View view = inflater.inflate(R.layout.timer_options, null);
        builder.setView(view);
        periodDurationPicker = view.findViewById(R.id.periodDurationPicker);
        periodDurationPicker.setMinValue(6);
        periodDurationPicker.setMaxValue(30);
        periodDurationPicker.setWrapSelectorWheel(false);
        periodDurationPicker.setValue(periodDurationMinutes);
        lineUpDurationPicker = view.findViewById(R.id.lineUpDurationPicker);
        lineUpDurationPicker.setMinValue(5);
        lineUpDurationPicker.setMaxValue(60);
        lineUpDurationPicker.setWrapSelectorWheel(false);
        lineUpDurationPicker.setValue(lineUpDurationSeconds);
        allowImmediateStartCheckBox = view.findViewById(R.id.allowImmediateStartCheckBox);
        allowImmediateStartCheckBox.setChecked(allowImmediateJamStart);
        builder.setPositiveButton(R.string.select, (dialog, id) -> {
            periodDurationMinutes = periodDurationPicker.getValue();
            lineUpDurationSeconds = lineUpDurationPicker.getValue();
            allowImmediateJamStart = allowImmediateStartCheckBox.isChecked();
            settingsListener.onDialogPositiveClick(TimerOptions.this);
        });
        builder.setNegativeButton(R.string.cancel, (dialog, id) -> {
            if (TimerOptions.this.getDialog() != null) {
                TimerOptions.this.getDialog().cancel();
            }
        });
        settings = builder.create();
        return settings;
    }


    public Bundle getState() {
        Bundle state = new Bundle();
        state.putInt("periodDuration", periodDurationMinutes);
        state.putInt("lineUpDuration", lineUpDurationSeconds);
        state.putBoolean("allowImmediateStart", allowImmediateJamStart);
        return state;
    }

    public void setValues(SharedPreferences prefs) {
        periodDurationMinutes = prefs.getInt("periodDuration", 30);
        lineUpDurationSeconds = prefs.getInt("lineUpDuration", 30);
        allowImmediateJamStart = prefs.getBoolean("allowImmediateStart", false);
    }

    @Override
    public void onAttach(@NonNull Activity activity) {
        super.onAttach(activity);
        try {
            settingsListener = (TimerOptions.SettingsListener)activity;
        } catch (ClassCastException ccex) {
            throw new ClassCastException(activity + " doesn't implement SettingsListener");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle state) {
        state.putInt("periodDuration", periodDurationMinutes);
        state.putInt("lineUpDuration", lineUpDurationSeconds);
        state.putBoolean("allowImmediateStart", allowImmediateJamStart);
        super.onSaveInstanceState(state);
    }

}
