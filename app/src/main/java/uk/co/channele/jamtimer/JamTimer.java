package uk.co.channele.jamtimer;

import android.content.SharedPreferences;
import android.os.*;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import static uk.co.channele.jamtimer.Activity.*;
import static uk.co.channele.jamtimer.Counter.FIVE_SECONDS;
import static uk.co.channele.jamtimer.Counter.ONE_MINUTE;
import static uk.co.channele.jamtimer.Counter.ONE_SECOND;
import static uk.co.channele.jamtimer.R.*;


public class JamTimer extends AppCompatActivity implements
        View.OnClickListener,
        PeriodClock.SettingsListener,
        TimerOptions.SettingsListener,
        ConfirmMasterReset.SettingsListener {

    private static final String TAG = "JamTimer";
    private static final int STANDARD_PERIOD_DURATION = 30;
    private static final int STANDARD_LINE_UP_DURATION = 30;
    private SharedPreferences appPrefs;
    private Vibrator vibe;
    private TimerViewModel model;
    // UI variables
    private Button actionButton;
    private Button timeoutTeam;
    private Button timeoutOfficial;
    private TextView periodDurationText;
    private TextView lineUpDurationText;
    private TextView periodTitle;
    private TextView jamTitle;
    private TextView periodClock;
    private TextView activityClock;
    private TextView activityTitle;
    // classes
    private Handler timer;
    private final Updater updater = new Updater();
    private PeriodClock periodClockAdjust;


    /**
     * Resets all counters statuses.
     */
    void masterReset() {
        model.masterReset();
        // titles
        activityTitle.setText(R.string.warmUp, TextView.BufferType.NORMAL);
        periodTitle.setText(getString(R.string.period, 0), TextView.BufferType.NORMAL);
        jamTitle.setText(getString(R.string.jam, 0), TextView.BufferType.NORMAL);
        // buttons
        actionButton.setText(string.lineup, TextView.BufferType.NORMAL);
        setButtonState(actionButton, true);
        setButtonState(timeoutOfficial, false);
        setButtonState(timeoutTeam, false);
        // clocks
        activityClock.setText(R.string.nullClock, TextView.BufferType.NORMAL);
        activityClock.setBackgroundResource(R.drawable.button);
        activityClock.setTextColor(Color.BLACK);
        periodClock.setText(R.string.nullClock, TextView.BufferType.NORMAL);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // FIXME: read docs and fix this
        model = ViewModelProvider.NewInstanceFactory.getInstance().create(TimerViewModel.class);
        appPrefs = getPreferences(MODE_PRIVATE);
        model.retrievePreferences(appPrefs);
        setContentView(R.layout.main);
        vibe = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
        periodTitle = findViewById(R.id.periodTitle);
        jamTitle = findViewById(R.id.jamTitle);
        periodDurationText = findViewById(R.id.periodDurationText);
        lineUpDurationText = findViewById(id.lineUpDurationText);
        setPeriodDurationAdvice();
        setLineUpDurationAdvice();
        periodClock = findViewById(R.id.periodClock);
        activityClock = findViewById(R.id.clock);
        activityTitle = findViewById(R.id.activityTitle);
        actionButton = findViewById(R.id.actionButton);
        timeoutTeam = findViewById(R.id.timeoutTeam);
        timeoutOfficial = findViewById(R.id.timeoutOfficial);
        actionButton.setOnClickListener(this);
        timeoutTeam.setOnClickListener(this);
        timeoutOfficial.setOnClickListener(this);
        masterReset();
        timer = new Handler();
        if (savedInstanceState == null) {
            checkBattery();
        }
    }


    /**
     * Handle player button clicks.
     *
     * <p>For the actionButton this method does the following:</p>
     * <ul>
     *     <li>set the currentActivity banner</li>
     *     <li>set the text of the actionButton to the next natural action</li>
     *     <li>set the activityClock value, colour and sets it running</li>
     *     <li>set the timeout button states</li>
     * </ul>
     * @param view the associated view
     */
    public void onClick(View view) {
        if (view.getId() == id.actionButton) {
            // Test the current activity, which should be reflected in the action indicated
            // by the actionButton. From this set the next activity.
            switch (model.getCurrentActivity()) {
                case INTERVAL:
                    // This should only occur after a timeout, allowing the timer to
                    // skip to five seconds rather than wait up to 30 seconds when
                    // the players may already be in position.
                    if (model.isTimeoutFlag() || model.getAllowImmediateStart()) {
                        setButtonState(actionButton, false);
                        activityClock.setBackgroundResource(R.drawable.button_yellow);
                        activityClock.setTextColor(Color.BLACK);
                        model.resetActivityCounter(FIVE_SECONDS, 0, true);
                        activityTitle.setText(R.string.fiveSeconds, TextView.BufferType.NORMAL);
                        activityClock.setText(model.getActivityDisplayTime(), TextView.BufferType.NORMAL);
                        model.clearTimeoutFlag();
                    }
                    break;
                case JAM:
                    // jam is running
                    // actionButton will be set to 'End Jam'
                    // IF period has expired then set halftime
                    // ELSE start 30 second interval counter
                    if (model.isPeriodCounterExpired()) {
                        if (model.getPeriod() == 1) {
                            setActivity(HALFTIME);
                        } else {
                            setActivity(NONE);
                        }
                    } else {
                        setActivity(INTERVAL);
                    }
                    break;
                case TEAM_TIMEOUT:
                case OFFICIAL_TIMEOUT:
                    // end timeout
                    setActivity(INTERVAL);
                    break;
                case HALFTIME:
                    // Start the second period.
                    model.setPeriod(2);
                    model.clearJamCount();
                    jamTitle.setText(getString(R.string.jam, model.getJam()), TextView.BufferType.NORMAL);
                    periodTitle.setText(getString(R.string.period, model.getPeriod()), TextView.BufferType.NORMAL);
                    model.resetPeriodCounter();
                    periodClock.setText(model.getPeriodDisplayTime(), TextView.BufferType.NORMAL);
                    setActivity(INTERVAL);
                    break;
                case NONE:
                default:
                    // This is before the bout starts and after it ends.
                    model.setPeriod(1);
                    periodTitle.setText(getString(R.string.period, model.getPeriod()), TextView.BufferType.NORMAL);
                    model.resetPeriodCounter();
                    periodClock.setText(model.getPeriodDisplayTime(), TextView.BufferType.NORMAL);
                    setActivity(INTERVAL);
            }
            if (!model.isTimeoutFlag()) {
                if (model.isPaused()) {
                    model.setPaused(false);
                    timer.postDelayed(updater, 100L);
                }
            }
        } else if (view.getId() == id.timeoutTeam) {
            // start activityCounter of 60 seconds
            setActivity(TEAM_TIMEOUT);
        } else if (view.getId() == id.timeoutOfficial) {
            // stop periodCounter, set activityCounter to 30 seconds
            setActivity(OFFICIAL_TIMEOUT);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_bar, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Method called when the activity is destroyed.
     * The callbacks must be destroyed here. Failure to do so results
     * in the counters running out too quickly as multiple callbacks are
     * made every tenth of a second.
     */
    @Override
    public void onDestroy() {
        timer.removeCallbacks(updater);
        super.onDestroy();
    }


    /**
     * Handle the Set button from the period clock adjustment dialog.
     * @param fragment the period clock adjustment fragment
     */
    public void onDialogPositiveClick(DialogFragment fragment) {
        if (fragment instanceof PeriodClock) {
            Bundle state = ((PeriodClock)fragment).getState();
            int periodMinutes = model.getPeriodDurationMinutes();
            int periodSeconds = 0;
            boolean resetIntervalCounter = false;
            periodMinutes = state.getInt("periodMinutes", periodMinutes);
            periodSeconds = state.getInt("periodSeconds", periodSeconds);
            resetIntervalCounter = state.getBoolean("resetIntervalCounter", resetIntervalCounter);
            model.resetPeriodCounterMinutesSeconds(periodMinutes, periodSeconds);
            periodClock.setText(model.getPeriodDisplayTime(), TextView.BufferType.NORMAL);
            if (resetIntervalCounter) {
                model.resetActivityCounter(model.getLineUpDurationSeconds() * ONE_SECOND, FIVE_SECONDS, false);
                activityClock.setText(model.getActivityDisplayTime(), TextView.BufferType.NORMAL);
            }
        } else if (fragment instanceof TimerOptions) {
            Bundle state = ((TimerOptions)fragment).getState();
            model.setPeriodDurationMinutes(state.getInt("periodDuration", 30));
            model.setLineUpDurationSeconds(state.getInt("lineUpDuration", 30));
            model.setAllowImmediateStart(state.getBoolean("allowImmediateStart", false));
            setPeriodDurationAdvice();
            setLineUpDurationAdvice();
            model.savePreferences(appPrefs);
        } else {
            timer.removeCallbacks(updater);
            masterReset();
        }
    }


    /**
     * Handle the cancel button from the period clock adjustment dialog.
     * @param fragment the period clock adjustment fragment
     */
    public void onDialogNegativeClick(DialogFragment fragment) {
        periodClockAdjust = null;
        model.setPaused(false);
        timer.post(updater);
    }


    /**
     * Handle action bar clicks
     * @param item the menu item clicked
     * @return true if the option is handled
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Bundle args;
        if (item.getItemId() == id.aboutApp) {
            AboutApp aboutApp = AboutApp.newInstance(2);
            aboutApp.show(getSupportFragmentManager(), getString(R.string.aboutApp));
        } else if (item.getItemId() == id.resetJam) {
            model.resetJam();
            timer.removeCallbacks(updater);
            activityClock.setText(model.getActivityDisplayTime(), TextView.BufferType.NORMAL);
            activityClock.setTextColor(Color.BLACK);
            activityClock.setBackgroundResource(R.drawable.button_yellow);
            periodClock.setText(model.getPeriodDisplayTime(), TextView.BufferType.NORMAL);
            activityTitle.setText(R.string.fiveSeconds, TextView.BufferType.NORMAL);
            actionButton.setText(R.string.ready, TextView.BufferType.NORMAL);
            setButtonState(actionButton, true);
            setButtonState(timeoutOfficial, true);
            setButtonState(timeoutTeam, true);
        } else if (item.getItemId() == id.adjustPeriodClock) {
            timer.removeCallbacks(updater);
            model.setPaused(true);
            args = new Bundle();
            int remainingMinutes = model.getPeriodRemaining() / ONE_MINUTE;
            int remainingSeconds = (model.getPeriodRemaining() / ONE_SECOND) - (remainingMinutes * 60);
            args.putInt("periodMinutes", remainingMinutes);
            args.putInt("periodSeconds", remainingSeconds);
            periodClockAdjust = PeriodClock.newInstance(2);
            periodClockAdjust.setArguments(args);
            periodClockAdjust.show(getSupportFragmentManager(), "ADJUST_PERIOD_CLOCK");
        } else if (item.getItemId() == id.masterReset) {
            ConfirmMasterReset confirm = ConfirmMasterReset.newInstance(2);
            confirm.show(getSupportFragmentManager(), "CONFIRM_RESET");
        } else if (item.getItemId() == id.setOptions) {
            TimerOptions timerOptions = TimerOptions.newInstance(3);
            timerOptions.setValues(appPrefs);
            timerOptions.show(getSupportFragmentManager(), "SETTINGS");
        } else {
            return super.onOptionsItemSelected(item);
        }
        return true;
    }


    /**
     * Set menu option states.
     *
     * @return always returns true
     */
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem resetJamTimer = menu.findItem(R.id.resetJam);
        MenuItem adjustPeriodClock = menu.findItem(R.id.adjustPeriodClock);
        MenuItem masterReset = menu.findItem(R.id.masterReset);
        MenuItem timerSettings = menu.findItem(id.setOptions);
        adjustPeriodClock.setEnabled(false);
        timerSettings.setEnabled(false);
        resetJamTimer.setEnabled(false);
        masterReset.setEnabled(true);
        if (model.getCurrentActivity() == JAM) {
            resetJamTimer.setEnabled(true);
            masterReset.setEnabled(false);
        }
        if (model.getCurrentActivity() == INTERVAL) {
            masterReset.setEnabled(false);
        }
        if (model.getCurrentActivity() == OFFICIAL_TIMEOUT) {
            adjustPeriodClock.setEnabled(true);
        }
        if (model.getCurrentActivity() == NONE) {
            timerSettings.setEnabled(true);
        }
        return true;
    }


    /**
     * Restore variables following a device configuration change.
     * (This is most likely an orientation change.)
     * Note that an immediate call is made to the updater. The accuracy of
     * the timer is affected by this, shortening the timed period by up to
     * a tenth of a second each time a configuration change occurs.
     * @param state the bundle of variables
     */
    @Override
    public void onRestoreInstanceState(Bundle state) {
        model.restoreState(state);
        if (state != null) {
            setActivity(model.getCurrentActivity());
            if (!model.isPaused()) {
                timer.post(updater);
            } else {
                if (model.getCurrentActivity() == INTERVAL) {
                    if (model.getActivityCounter().isLowLevel()) {
                        vibe.vibrate(VibrationEffect.createOneShot(100, VibrationEffect.DEFAULT_AMPLITUDE));
                        activityClock.setBackgroundResource(R.drawable.button_yellow);
                        activityTitle.setText(R.string.fiveSeconds, TextView.BufferType.NORMAL);
                        actionButton.setText(R.string.ready, TextView.BufferType.NORMAL);
                        setButtonState(actionButton, false);
                    }
                }
            }
        }
    }


    /**
     * Save state before device configuration change. Android will handle
     * all views, so we just deal with internal variables.
     *
     * @param state the bundle in which to save variable states
     */
    @Override
    public void onSaveInstanceState(@NonNull Bundle state) {
        model.saveInstanceState(state);
        super.onSaveInstanceState(state);
    }


    @Override
    public void onStop() {
        super.onStop();
        model.savePreferences(appPrefs);
    }


    /**
     * Set the app button/clock state for the given activity.
     *
     * <p>Note that it is the responsibility of the caller to determine the
     * current activity and request the appropriate new activity.</p>
     */
    void setActivity(Activity activity) {
        model.setCurrentActivity(activity);
        switch (model.getCurrentActivity()) {
            case INTERVAL:
                activityTitle.setText(R.string.lineup, TextView.BufferType.NORMAL);
                actionButton.setText(string.fiveSeconds, TextView.BufferType.NORMAL);
                activityClock.setTextColor(Color.BLACK);
                activityClock.setBackgroundResource(R.drawable.button);
                if (model.isTimeoutFlag() || model.getAllowImmediateStart()) {
                    actionButton.setText(R.string.fiveSeconds, TextView.BufferType.NORMAL);
                    setButtonState(actionButton, true);
                } else {
                    setButtonState(actionButton, false);
                }
                setButtonState(timeoutOfficial, true);
                setButtonState(timeoutTeam, true);
                break;
            case JAM:
                jamTitle.setText(getString(R.string.jam, model.getJam()), TextView.BufferType.NORMAL);
                activityTitle.setText(R.string.jamOn, TextView.BufferType.NORMAL);
                actionButton.setText(R.string.endJam, TextView.BufferType.NORMAL);
                setButtonState(actionButton, true);
                setButtonState(timeoutOfficial, false);
                setButtonState(timeoutTeam, false);
                activityClock.setBackgroundResource(R.drawable.button_green);
                activityClock.setTextColor(Color.BLACK);
                break;
            case TEAM_TIMEOUT:
                activityTitle.setText(R.string.timeoutTeam, TextView.BufferType.NORMAL);
                actionButton.setText(string.lineup, TextView.BufferType.NORMAL);
                setButtonState(actionButton, true);
                setButtonState(timeoutOfficial, true);
                setButtonState(timeoutTeam, false);
                activityClock.setBackgroundResource(R.drawable.button);
                activityClock.setTextColor(Color.BLACK);
                break;
            case OFFICIAL_TIMEOUT:
                timer.removeCallbacks(updater);
                activityTitle.setText(R.string.timeoutOfficial, TextView.BufferType.NORMAL);
                actionButton.setText(R.string.restart, TextView.BufferType.NORMAL);
                setButtonState(actionButton, true);
                setButtonState(timeoutOfficial, false);
                setButtonState(timeoutTeam, false);
                activityClock.setBackgroundResource(R.drawable.button_red);
                activityClock.setTextColor(Color.WHITE);
                break;
            case HALFTIME:
                activityTitle.setText(R.string.halfTime, TextView.BufferType.NORMAL);
                actionButton.setText(string.lineup, TextView.BufferType.NORMAL);
                setButtonState(actionButton, true);
                setButtonState(timeoutOfficial, false);
                setButtonState(timeoutTeam, false);
                activityClock.setBackgroundResource(R.drawable.button);
                activityClock.setTextColor(Color.WHITE);
                break;
            case NONE:
            default:
                setButtonState(actionButton, false);
                setButtonState(timeoutOfficial, false);
                setButtonState(timeoutTeam, false);
                activityClock.setBackgroundResource(R.drawable.button);
                activityClock.setTextColor(Color.WHITE);
                activityClock.setText(R.string.nullClock, TextView.BufferType.NORMAL);
                break;
        }
        activityClock.setText(model.getActivityDisplayTime(), TextView.BufferType.NORMAL);
    }


    /**
     * Sets the state of the button to enable/disabled.
     *
     * @param button the button to set
     * @param enabled true to enable, false to disable
     */
    void setButtonState(Button button, boolean enabled) {
        button.setClickable(enabled);
        if (enabled) {
            button.setTextColor(Color.BLACK);
            button.setBackgroundResource(R.drawable.button_enabled);
        } else {
            button.setBackgroundResource(R.drawable.button_disabled);
            button.setTextColor(Color.WHITE);
        }
    }


    /**
     * A Runnable class required to handle callbacks.
     *
     * <p>Callbacks are made every tenth of a second. If the counters are 'stopped'
     * then no further action is taken. Otherwise each of the Counters
     * in the array is reviewed. The usual action is to decrement the count
     * by one. Button titles are reset when counters run out. Jammer status is
     * reviewed. Once all counters have run out 'stopped' state is set.</p>
     */
    class Updater implements Runnable {

        public void run() {
            boolean timerRunning = false;
            long timerDelay = 100L;
            if (!model.isPaused()) {
                if (model.isPeriodCounterRunning()) {
                    timerRunning = true;
                    model.decrementPeriodCounter();
                    if (model.isPeriodCounterDisplayTimeChanged()) {
                        periodClock.setText(model.getPeriodDisplayTime(), TextView.BufferType.NORMAL);
                    }
                }
                if (model.isActivityCounterRunning()) {
                    timerRunning = true;
                    if (model.isActivityCounterDisplayTimeChanged()) {
                        activityClock.setText(model.getActivityDisplayTime(), TextView.BufferType.NORMAL);
                        if (model.isActivityCounterWarningLevel()) {
                            vibe.vibrate(VibrationEffect.createOneShot(100, VibrationEffect.DEFAULT_AMPLITUDE));
                            activityClock.setBackgroundResource(R.drawable.button_yellow);
                            if (model.getCurrentActivity() == INTERVAL) {
                                activityTitle.setText(R.string.fiveSeconds, TextView.BufferType.NORMAL);
                                actionButton.setText(R.string.ready, TextView.BufferType.NORMAL);
                                setButtonState(actionButton, false);
                            }
                        }
                    }
                    if (model.isActivityCounterExpired()) {
                        if (!model.isPeriodCounterRunning()) {
                            timerDelay = 0L;
                        }
                        // this detects the JUST ENDED activity
                        switch (model.getCurrentActivity()) {
                            case JAM:
                                if (model.isPeriodCounterExpired()) {
                                    switch (model.getPeriod()) {
                                        case (1) :
                                            setActivity(HALFTIME);
                                            break;
                                        case (2) :
                                            actionButton.setText(R.string.gameOver, TextView.BufferType.NORMAL);
                                            activityTitle.setText(R.string.gameOver, TextView.BufferType.NORMAL);
                                            setActivity(NONE);
                                            break;
                                    }
                                } else {
                                    setActivity(INTERVAL);
                                }
                                break;
                            case INTERVAL:
                                if (model.isPeriodCounterExpired()) {
                                    switch (model.getPeriod()) {
                                        case (1) :
                                            setActivity(HALFTIME);
                                            break;
                                        case(2) :
                                            model.setCurrentActivity(NONE);
                                            break;
                                    }
                                } else {
                                    if (!model.isPeriodCounterRunning()) {
                                        model.startPeriodCounter();
                                    }
                                    model.clearTimeoutFlag();
                                    model.incrementJam();
                                    setActivity(JAM);
                                }
                                break;
                            case TEAM_TIMEOUT:
                                setActivity(INTERVAL);
                                break;
                            case HALFTIME:
                                model.setPeriod(2);
                                model.clearJamCount();
                                jamTitle.setText(getString(R.string.jam, model.getJam()), TextView.BufferType.NORMAL);
                                periodTitle.setText(getString(R.string.period, model.getPeriod()), TextView.BufferType.NORMAL);
                                model.resetPeriodCounter();
                                periodClock.setText(model.getPeriodDisplayTime(), TextView.BufferType.NORMAL);
                                setActivity(INTERVAL);
                                break;
                        }
                    }
                    model.decrementActivityCounter();
                }
                if (timerRunning) {
                    timer.postDelayed(updater, timerDelay);
                } else {
                    model.setPaused(true);
                }
            }
        }
    }

    private void setPeriodDurationAdvice() {
        periodDurationText.setText(
                getString(string.periodDurationAdvice, model.getPeriodDurationMinutes()),
                TextView.BufferType.NORMAL);
        if (model.getPeriodDurationMinutes() != STANDARD_PERIOD_DURATION) {
            periodDurationText.setBackgroundColor(Color.parseColor("#FFFFF176"));
            periodDurationText.setTextColor(Color.parseColor("#FF000000"));
        } else {
            periodDurationText.setBackgroundColor(Color.parseColor("#00000000"));
            periodDurationText.setTextColor(Color.parseColor("#66000000"));
        }
    }


    private void setLineUpDurationAdvice() {
        lineUpDurationText.setText(
                getString(string.lineUpDurationAdvice, model.getLineUpDurationSeconds()),
                TextView.BufferType.NORMAL);
        if (model.getLineUpDurationSeconds() != STANDARD_LINE_UP_DURATION) {
            lineUpDurationText.setBackgroundColor(Color.parseColor("#FFFFF176"));
            lineUpDurationText.setTextColor(Color.parseColor("#FF000000"));
        } else {
            lineUpDurationText.setBackgroundColor(Color.parseColor("#00000000"));
            lineUpDurationText.setTextColor(Color.parseColor("#66000000"));
        }
    }


    /**
     * Check that the battery level is reasonable and warn if not.
     */
    private void checkBattery() {
        IntentFilter intentFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatusIntent = this.registerReceiver(null, intentFilter);
        if (batteryStatusIntent != null) {
            int status = batteryStatusIntent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
            if (status == BatteryManager.BATTERY_STATUS_DISCHARGING ||
                    status == BatteryManager.BATTERY_STATUS_NOT_CHARGING) {
                int batLevel = batteryStatusIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
                int maxLevel = batteryStatusIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
                int batPercent = batLevel * 100 / maxLevel;
                if (batPercent < 20) {
                    Toast.makeText(this, "Battery level is low!", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

}