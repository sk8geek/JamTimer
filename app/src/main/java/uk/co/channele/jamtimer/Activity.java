package uk.co.channele.jamtimer;

public enum Activity {
    NONE, INTERVAL, JAM, TEAM_TIMEOUT, OFFICIAL_TIMEOUT, HALFTIME
}
